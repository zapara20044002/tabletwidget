from PyQt5.QtWidgets import QMainWindow, QTableWidgetItem, QMessageBox
from PyQt5.QtWidgets import QFileDialog
from ui_py import ui_main_window
from ui_py import ui_main_open
from windows import dialog_add_edit
import csv


class MainWindow(QMainWindow, ui_main_window.Ui_MainWindow, ui_main_open.Ui_MainWindow):
    def __init__(self):
        super().__init__()
        self.setupUi(self)
        self.pushButton_add.clicked.connect(self.btn_add_click)
        self.pushButton_del.clicked.connect(self.btn_delete_click)
        self.pushButton_edit.clicked.connect(self.btn_edit_click)
        self.pushButton_fromFile.clicked.connect(self.btn_from_file_click)
        # self.pushButton_toFile.clicked.connect(self.btn_to_file_click)
        self.show_csv.clicked.connect(self.btn_csvView)
        self.btn_add_csv.clicked.connect(self.btn_csvView)

    def btn_add_click(self):
        dlg = dialog_add_edit.DialogAddEditStudent(self, mode='add', app_name=self.windowTitle())
        dlg.show()
        res = dlg.exec_()
        if res:
            data = dlg.get_data()
            row = self.tableWidget.rowCount()
            self.tableWidget.setRowCount(row + 1)
            self.tableWidget.setItem(row, 0, QTableWidgetItem(data['lastName']))
            self.tableWidget.setItem(row, 1, QTableWidgetItem(data['firstName']))
            self.tableWidget.setItem(row, 2, QTableWidgetItem(data['middleName']))
            self.tableWidget.setItem(row, 3, QTableWidgetItem(data['group']))

    def btn_delete_click(self):
        msg = QMessageBox.question(self,
                                   self.windowTitle(),
                                   'Вы действительно хотите удалить запись?',
                                   QMessageBox.Yes | QMessageBox.No)
        if msg == QMessageBox.Yes:
            row = self.tableWidget.currentRow()
            self.tableWidget.removeRow(row)

    def closeEvent(self, event):
        close = QMessageBox()
        close.setText("Вы уверены, что хотите выйти?")
        close.setStandardButtons(QMessageBox.Yes | QMessageBox.Cancel)
        close = close.exec_()

        if close == QMessageBox.Yes:
            event.accept()
        else:
            event.ignore()



    def btn_edit_click(self):
        row = self.tableWidget.currentRow()
        if row != -1:
            data = {
                'lastName': self.tableWidget.item(row, 0).text(),
                'firstName': self.tableWidget.item(row, 1).text(),
                'middleName': self.tableWidget.item(row, 2).text(),
                'group': self.tableWidget.item(row, 3).text(),
            }
            dlg = dialog_add_edit.DialogAddEditStudent(self,
                                                       mode='edit',
                                                       app_name=self.windowTitle(),
                                                       data=data)
            dlg.show()
            res = dlg.exec_()
            if res:
                data = dlg.get_data()
                self.tableWidget.item(row, 0).setText(data['lastName'])
                self.tableWidget.item(row, 1).setText(data['firstName'])
                self.tableWidget.item(row, 2).setText(data['middleName'])
                self.tableWidget.item(row, 3).setText(data['group'])
        else:
            QMessageBox.warning(self,
                                self.windowTitle(),
                                'Строка для редактирования не выбрана!')

    def btn_from_file_click(self):
        fd = QFileDialog.getOpenFileName(self,
                                         f'{self.windowTitle()} [открыть]',
                                         '',
                                         'CSV (*.csv);;txt (*.txt);;All (*.*)')
        with open(fd[0], 'r', encoding='utf-8') as f:
            reader = csv.reader(f, delimiter=',')
            elems = list(reader)
        elems.pop(0)
        for data in elems:
            row = self.tableWidget.rowCount()
            self.tableWidget.setRowCount(row + 1)
            self.tableWidget.setItem(row, 0, QTableWidgetItem(data[0]))
            self.tableWidget.setItem(row, 1, QTableWidgetItem(data[1]))
            self.tableWidget.setItem(row, 2, QTableWidgetItem(data[2]))
            self.tableWidget.setItem(row, 3, QTableWidgetItem(data[3]))

    def btn_csvView(self):
        res_dlg = QFileDialog.getOpenFileName(self,
                                              self.windowTitle(),
                                              filter='CSV файлы (*.csv)')
        if res_dlg[0]:
            with open(res_dlg[0], 'r', encoding='utf-8') as f:
                rd = csv.DictReader(f, delimiter=';')
                self.tableWidget.clear()
                headers = rd.fieldnames
                self.tableWidget.setColumnCount(len(headers))
                self.tableWidget.setHorizontalHeaderLabels(headers)
                for i, row in enumerate(rd):
                    for j, (key, value) in enumerate(row.items()):
                        self.tableWidget.setItem(i, f, QTableWidgetItem(value))
