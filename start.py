from PyQt5.QtWidgets import QApplication, QMessageBox
from windows import main_window

if __name__ == '__main__':
    app = QApplication([])
    hello = QMessageBox(QMessageBox.Question, 'Добро пожаловать!', "Здравствуйте!")
    hello.addButton(QMessageBox.Ok)
    wnd = main_window.MainWindow()
    hello.show()
    wnd.show()
    app.exec_()